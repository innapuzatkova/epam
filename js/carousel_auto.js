var slideIndex = 0;

autoShowSlides();

function autoShowSlides() {
    var i;
    var slides = document.getElementsByClassName("slider-container");
    var dots = document.getElementsByClassName("dot");
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }

    slideIndex++;
    if (slideIndex > slides.length) {
        slideIndex = 1;
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active-slider", "");
    }
    slides[slideIndex-1].style.display = "block";
    dots[slideIndex-1].className += " active-slider";
    setTimeout(showSlides, 2000);
}
